package com.sourceit.firstandroidproject;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView helloWorld;
    EditText inputText;
    Button action;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        helloWorld = (TextView) findViewById(R.id.hello_world);
        inputText = (EditText) findViewById(R.id.input_text);
        action = (Button) findViewById(R.id.action);

        helloWorld.setText(R.string.hello_sourceit);
        helloWorld.setTextColor(Color.BLUE);
        helloWorld.setAllCaps(true);

        action.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String newLine = inputText.getText().toString();
                        helloWorld.setText(newLine);
                    }
                }
        );

//        action.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        String newLine = inputText.getText().toString();
        helloWorld.setText(newLine);
    }
}
